package com.gitlab.redstonerevive.anonymousanalytics;

import org.bukkit.plugin.java.JavaPlugin;

public final class AnonymousAnalytics extends JavaPlugin {

    /**
     * The active instance of AnonymousAnalytics.
     */
    private static AnonymousAnalytics instance = null;

    /**
     * A static accessor for the active AnonymousAnalytics plugin.
     */
    public static AnonymousAnalytics getInstance () {
        return instance;
    }

    /**
     * Called when the plugin is enabled.
     */
    @Override
    public void onEnable () {
        instance = this;
    }
}
