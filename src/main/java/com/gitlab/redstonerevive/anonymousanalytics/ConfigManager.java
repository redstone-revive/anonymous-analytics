package com.gitlab.redstonerevive.anonymousanalytics;

import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ConfigManager handles the config file and allows accessing the values from a singleton instance.
 *
 * @author Sugaku
 */
public class ConfigManager {

    /**
     * The active ConfigManager instance at runtime.
     */
    private static ConfigManager instance = null;

    /**
     * Whether the ActiveUsers section is enabled or not.
     */
    @Getter
    private boolean activeUserTracking = true;

    /**
     * Creates a new ConfigManager by saving default values and loading any new ones.
     */
    private ConfigManager () {
        AnonymousAnalytics plugin = AnonymousAnalytics.getInstance();
        Logger logger = plugin.getLogger();
        plugin.saveDefaultConfig();
        try {
            FileConfiguration config = YamlConfiguration.loadConfiguration(new File("./plugins/AnonymousAnalytics/config.yml"));
            activeUserTracking = config.getBoolean("active-users", true);
        } catch (Exception exception) {
            logger.log(Level.SEVERE, "Something went wrong: " + exception.getMessage());
        }
    }

    /**
     * A static accessor method for the active ConfigManager instance.
     *
     * @return The active ConfigManager instance.
     */
    public static ConfigManager getInstance () {
        if (instance == null) instance = new ConfigManager();
        return instance;
    }
}
